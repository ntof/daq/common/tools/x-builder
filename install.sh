#!/bin/bash
# Author: Sylvain Fargier <sylvain.fargier@cern.ch>

: ${DESTDIR:=~/}
CFG_DIR=$(cd $(dirname $0); pwd)

. "${CFG_DIR}/scripts/.functions.sh"

cd "$DESTDIR" || die "Can't find \"${DESTDIR}\""

function realpath() {
  FILE="$1"
  while [ -L "$FILE" ]; do
    FILE=$(readlink "$FILE")
  done
  DIR=$(dirname "$FILE")
  DIR=$(cd "$DIR"; pwd -P)
  FILENAME=$(basename "$FILE")
  echo "$DIR/$FILENAME"
}

function mksym()
{
  [ "$#" -ne 2 ] && die "Usage: mksym cfg dest"

  CFG="$(realpath ${CFG_DIR}/$1)"
  DEST="$2"

  ebegin "Installing \"$2\""
  if [ -e "$DEST" ]; then
    if [ -L "$DEST" ] && [ $(readlink "$DEST") == "$CFG" ]; then
      eend 0
    else
      eend 1 "config file exists \"$DEST\""
    fi
  else
    ln -s "$CFG" "$DEST" || eend 1 "failed to create symlink \"$DEST\"" && eend 0
  fi
}

mkdir -p "${DESTDIR}/bin"
mksym 'scripts/x-builder' 'bin/x-builder'
mksym 'scripts/ssh-builder' 'bin/ssh-builder'
mksym 'scripts/ssh-builder-sync' 'bin/ssh-builder-sync'
mksym 'scripts/docker-builder' 'bin/docker-builder'
mksym 'scripts/.functions.sh' 'bin/.functions.sh'

mksym 'shortcuts/x-builder-clang-format' 'bin/x-builder-clang-format'
mksym 'shortcuts/x-builder-cppcheck' 'bin/x-builder-cppcheck'
mksym 'shortcuts/x-builder-clang' 'bin/x-builder-clang'
mksym 'shortcuts/x-builder-clangd' 'bin/x-builder-clangd'

mksym 'shortcuts/vsim' 'bin/vsim'
mksym 'shortcuts/vsim' 'bin/vlog'
mksym 'shortcuts/vsim' 'bin/verible-verilog-format'
mksym 'shortcuts/vsim' 'bin/verible-verilog-lint'
mksym 'shortcuts/vsim' 'bin/verible-verilog-ls'

mksym 'shortcuts/vitis' 'bin/vitis'
mksym 'shortcuts/vitis' 'bin/vivado'
